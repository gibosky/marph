package br.com.marph.modelo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
public class Pessoa implements Serializable {
    private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String nome;
	
	private String cpf;
	
	@Temporal(TemporalType.DATE)
	private Calendar nascimento = Calendar.getInstance();
	
	@OneToMany(mappedBy="pessoa")
	private List<Endereco> enderecos;
	
	public Pessoa(){}
	
	public Pessoa(int id, String nome, String cpf, Calendar nascimento,
			List<Endereco> enderecos) {
		this.id = id;
		this.nome = nome;
		this.setCpf(cpf);
		this.nascimento = nascimento;
	}

	@Override
	public Pessoa clone() {
		return new Pessoa(id, nome, getCpf(), nascimento, enderecos);
	}
	
	public List<Endereco> getEnderecos() {
		return enderecos;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Calendar getNascimento() {
		return nascimento;
	}
	public void setNascimento(Calendar nascimento) {
		this.nascimento = nascimento;
	}
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}	
}
