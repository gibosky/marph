package br.com.marph.test;

import javax.persistence.EntityManager;

import br.com.marph.dao.JPAUtil;


public class TestBanco {

	public static void main(String[] args) {
		
		String param = "4";
	
		EntityManager manager = new JPAUtil().getEntityManager();
		manager.getTransaction().begin();
		manager.createQuery("DELETE FROM Usuario u WHERE u.pessoa.id = " + param).executeUpdate();
		manager.createQuery("DELETE FROM Endereco e WHERE e.pessoa.id = " + param ).executeUpdate();
		manager.createQuery("DELETE FROM Pessoa p WHERE p.id = " + param ).executeUpdate();
		manager.getTransaction().commit();
		manager.close();
		
	}

	
}
