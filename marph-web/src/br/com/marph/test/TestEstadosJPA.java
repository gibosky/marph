package br.com.marph.test;

import java.util.Calendar;

import javax.persistence.EntityManager;

import br.com.marph.dao.JPAUtil;
import br.com.marph.modelo.Pessoa;

public class TestEstadosJPA {

	public static void main(String[] args) {

		EntityManager em = new JPAUtil().getEntityManager();
		
		em.getTransaction().begin();
		
		Pessoa pessoa = em.find(Pessoa.class, 1);
		System.out.println(pessoa.getNome());
		
		System.out.println(pessoa.getCpf());
		pessoa.setCpf("12345678900");
		
		System.out.println(pessoa.getNascimento());
		pessoa.setNascimento(Calendar.getInstance());
		
		em.getTransaction().commit();
		
		em.close();
	}

}
