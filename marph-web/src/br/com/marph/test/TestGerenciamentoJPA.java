package br.com.marph.test;

import javax.persistence.EntityManager;

import br.com.marph.dao.JPAUtil;
import br.com.marph.modelo.Pessoa;

public class TestGerenciamentoJPA {

	public static void main(String[] args) {
		
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		
		Pessoa pessoa = em.find(Pessoa.class, 1);
		em.getTransaction().commit();
		
		pessoa.setNome("Carla Maria");
		
		em.getTransaction().begin();
		em.merge(pessoa);
		em.getTransaction().commit();
		
		em.close();
	}
}
