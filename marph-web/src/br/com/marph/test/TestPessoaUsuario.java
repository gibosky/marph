package br.com.marph.test;

import javax.persistence.EntityManager;

import br.com.marph.modelo.Pessoa;
import br.com.marph.modelo.Usuario;
import br.com.marph.dao.JPAUtil;

public class TestPessoaUsuario {

	public static void main(String[] args) {

		EntityManager em = new JPAUtil().getEntityManager();
		
		em.getTransaction().begin();
		Usuario usuario = em.find(Usuario.class, 1);
		
		Pessoa pessoa = em.find(Pessoa.class, usuario.getPessoa().getId());
		
		System.out.println(pessoa.getNome());
		
		em.close();
	}

}
