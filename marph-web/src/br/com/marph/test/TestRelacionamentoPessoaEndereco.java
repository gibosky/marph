package br.com.marph.test;

import java.util.Calendar;

import javax.persistence.EntityManager;

import br.com.marph.modelo.Endereco;
import br.com.marph.modelo.Pessoa;
import br.com.marph.dao.JPAUtil;

public class TestRelacionamentoPessoaEndereco {

	public static void main(String[] args) {

		EntityManager em = new JPAUtil().getEntityManager();
		
		try {
		
			double inicio = System.currentTimeMillis();
			
			Pessoa pessoa = new Pessoa();
			pessoa.setNome("Eduardo");
			pessoa.setCpf("12345678900");
			pessoa.setNascimento(Calendar.getInstance());
			
			Endereco endereco = new Endereco();
			endereco.setBairro("California");
			endereco.setPessoa(pessoa);
			endereco.setRua("Rua Um");
			endereco.setTelefone("3456-7654");
			
			Endereco endereco2 = new Endereco();
			endereco2.setBairro("Sao Paulo");
			endereco2.setPessoa(pessoa);
			endereco2.setRua("Rua Dois");
			endereco2.setTelefone("7383-9303");
			
			em.getTransaction().begin();
			em.persist(pessoa);
			em.persist(endereco);
			em.persist(endereco2);
			em.getTransaction().commit();
			
			double fim = System.currentTimeMillis();
			System.out.println("Executado em: " + (fim - inicio) / 1000 + "s");
			
		} finally {
			em.close();
		}
	}

}
