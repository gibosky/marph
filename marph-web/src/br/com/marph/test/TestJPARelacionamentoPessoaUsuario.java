package br.com.marph.test;

import java.util.Calendar;

import javax.persistence.EntityManager;

import br.com.marph.dao.JPAUtil;
import br.com.marph.modelo.Pessoa;
import br.com.marph.modelo.Usuario;

public class TestJPARelacionamentoPessoaUsuario {

	public static void main(String[] args) {

		double inicio = System.currentTimeMillis();
		
		Pessoa pessoa = new Pessoa();
		pessoa.setNome("Carla");
		pessoa.setCpf("12345678900");
		pessoa.setNascimento(Calendar.getInstance());
		
		Usuario usuario = new Usuario();
		usuario.setLogin("carlinha");
		usuario.setPessoa(pessoa);
		usuario.setSenha("123456");
		usuario.setSituacao(false);
		
		EntityManager em = new JPAUtil().getEntityManager();
		
		em.getTransaction().begin();
		em.persist(pessoa);
		em.persist(usuario);
		em.getTransaction().commit();
		em.close();
		
		double fim = System.currentTimeMillis();
		System.out.println("Executado em: " + (fim - inicio) / 1000 + "s");
	}

}
