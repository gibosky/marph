package br.com.marph.test;

import java.util.Calendar;

import javax.persistence.EntityManager;

import br.com.marph.dao.JPAUtil;
import br.com.marph.modelo.Pessoa;

public class TestJPA {

	public static void main(String[] args) {

		double inicio = System.currentTimeMillis();
		
		Pessoa pessoa = new Pessoa();
		pessoa.setNome("Carla");
		pessoa.setCpf("12345678900");
		pessoa.setNascimento(Calendar.getInstance());
		
		EntityManager em = new JPAUtil().getEntityManager();
		
		em.getTransaction().begin();
		em.persist(pessoa);
		em.getTransaction().commit();
		em.close();
		
		double fim = System.currentTimeMillis();
		System.out.println("Executado em: " + (fim - inicio) / 1000 + "s");
	}

}
