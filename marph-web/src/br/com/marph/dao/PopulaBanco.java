package br.com.marph.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.EntityManager;

import br.com.marph.modelo.Endereco;
import br.com.marph.modelo.Pessoa;
import br.com.marph.modelo.Usuario;

public class PopulaBanco {

	public static void main(String[] args) {

		EntityManager em = new JPAUtil().getEntityManager();

		em.getTransaction().begin();
		
		Pessoa pessoa = new Pessoa();
		pessoa.setNome("Carla");
		pessoa.setCpf("12345678900");
		pessoa.setNascimento(Calendar.getInstance());
		
		Pessoa pessoa2 = new Pessoa();
		pessoa2.setNome("Rafaela");
		pessoa2.setCpf("09876543211");
		pessoa2.setNascimento(Calendar.getInstance());
	
		Pessoa pessoa3 = new Pessoa();
		pessoa3.setNome("Marcos");
		pessoa3.setCpf("01929394856");
		pessoa3.setNascimento(Calendar.getInstance());

		em.persist(pessoa);
		em.persist(pessoa2);
		em.persist(pessoa3);
		
		Endereco endereco = new Endereco();
		endereco.setRua("Rua um");
		endereco.setBairro("Bairro dois");
		endereco.setTelefone("3456-7890");
		endereco.setPessoa(pessoa);
		
		Endereco endereco2 = new Endereco();
		endereco2.setRua("Rua tres");
		endereco2.setBairro("Bairro quatro");
		endereco2.setTelefone("3456-7890");
		endereco2.setPessoa(pessoa2);
		
		Endereco endereco3 = new Endereco();
		endereco3.setRua("Rua cinco");
		endereco3.setBairro("Bairro seis");
		endereco3.setTelefone("3456-7890");
		endereco3.setPessoa(pessoa3);
		
		em.persist(endereco);
		em.persist(endereco2);
		em.persist(endereco3);
		
		Usuario usuario = new Usuario();
		usuario.setLogin("carlinha");
		usuario.setSenha("12345678");
		usuario.setSituacao(false);
		usuario.setPessoa(pessoa);
		
		Usuario usuario2 = new Usuario();
		usuario2.setLogin("rafinha");
		usuario2.setSenha("9876543");
		usuario2.setSituacao(false);
		usuario2.setPessoa(pessoa2);
		
		Usuario usuario3 = new Usuario();
		usuario3.setLogin("markin");
		usuario3.setSenha("terwere");
		usuario3.setSituacao(false);
		usuario3.setPessoa(pessoa3);
		
		em.persist(usuario);
		em.persist(usuario2);
		em.persist(usuario3);
		
		em.getTransaction().commit();
		
		em.close();

	}
	@SuppressWarnings("unused")
	private static Calendar parseData(String data) {
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(data);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return calendar;
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
	}

}
