package br.com.marph.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.Query;

import br.com.marph.modelo.Endereco;
import br.com.marph.modelo.Usuario;

public class DAO<T> {

	private final Class<T> classe;

	public DAO(Class<T> classe) {
		this.classe = classe;
	}
	
	public void adiciona(T t) {

		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		em.close();
	}

	public void remove(T t) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		em.remove(em.merge(t));
		em.getTransaction().commit();
		em.close();
	}
	
	public void removePessoa(String idPessoa) {
		EntityManager manager = new JPAUtil().getEntityManager();
		manager.getTransaction().begin();
		manager.createQuery("DELETE FROM Usuario u WHERE u.pessoa.id = " + idPessoa).executeUpdate();
		manager.createQuery("DELETE FROM Endereco e WHERE e.pessoa.id = " + idPessoa ).executeUpdate();
		manager.createQuery("DELETE FROM Pessoa p WHERE p.id = " + idPessoa ).executeUpdate();
		manager.getTransaction().commit();
		manager.close();
	}

	public void atualiza(T t) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		em.merge(t);
		em.getTransaction().commit();
		em.close();
	}

	public List<T> listaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(classe);
		query.select(query.from(classe));
		List<T> lista = em.createQuery(query).getResultList();
		em.close();
		return lista;
	}
	
	public Usuario procuraUsuarioPorIdPessoa(String idPessoa) {
		EntityManager manager = new JPAUtil().getEntityManager();
		manager.getTransaction().begin();
		Query query = manager.createQuery("SELECT u FROM Usuario u WHERE u.pessoa.id = " + idPessoa);
		Usuario singleResult = (Usuario) query.getSingleResult();
		manager.close();
		return singleResult;
	}

	public Endereco procuraEnderecoPorIdPessoa(String idPessoa) {
		EntityManager manager = new JPAUtil().getEntityManager();
		manager.getTransaction().begin();
		Query query = manager.createQuery("SELECT u FROM Endereco u WHERE u.pessoa.id = " + idPessoa);
		Endereco singleResult = (Endereco) query.getSingleResult();
		manager.close();
		return singleResult;
	}
}