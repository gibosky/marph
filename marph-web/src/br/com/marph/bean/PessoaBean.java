package br.com.marph.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.com.marph.dao.DAO;
import br.com.marph.modelo.Endereco;
import br.com.marph.modelo.Pessoa;
import br.com.marph.modelo.Usuario;

@ManagedBean
@SessionScoped
public class PessoaBean implements Serializable {
    private static final long serialVersionUID = 1L;

	Pessoa pessoa = new Pessoa();
	Endereco endereco = new Endereco();
	Usuario usuario = new Usuario();
	
    private boolean edit = false;
    
	public boolean isEdit() {
		return edit;
	}
	private List<Pessoa> listaPessoas;
	
	public Pessoa getPessoa() {
		return pessoa;
	}

	public Endereco getEndereco() {
		return endereco;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	@PostConstruct
    public void init() {
        this.listaPessoas = new ArrayList<Pessoa>();
        this.pessoa = new Pessoa();
		this.endereco = new Endereco();
		this.usuario = new Usuario();
    }
	
	public List<Pessoa> getPessoas() {
		this.listaPessoas = new DAO<Pessoa>(Pessoa.class).listaTodos();
		return this.listaPessoas;
	} 
	
	public String salvar() {
		if (! this.edit) {
			new DAO<Pessoa>(Pessoa.class).adiciona(this.pessoa);
			this.endereco.setPessoa(this.pessoa);
			new DAO<Endereco>(Endereco.class).adiciona(this.endereco);
			this.usuario.setPessoa(this.pessoa);
			new DAO<Usuario>(Usuario.class).adiciona(this.usuario);
				
		} else {
			new DAO<Pessoa>(Pessoa.class).atualiza(this.pessoa);
			this.endereco.setPessoa(this.pessoa);
			new DAO<Endereco>(Endereco.class).atualiza(this.endereco);
			this.usuario.setPessoa(this.pessoa);
			new DAO<Usuario>(Usuario.class).atualiza(this.usuario);
			this.edit = false;
		}
		this.pessoa = new Pessoa();
		this.endereco = new Endereco();
		this.usuario = new Usuario();
		return "home?faces-redirect=true";
	}
	
	public String editar(Pessoa item) {
        edit = true;
        this.pessoa  = item.clone();
        this.usuario = new DAO<Usuario>(Usuario.class).procuraUsuarioPorIdPessoa(Integer.toString(this.pessoa.getId()));
        this.endereco = new DAO<Endereco>(Endereco.class).procuraEnderecoPorIdPessoa(Integer.toString(this.pessoa.getId()));
        return "salvar?faces-redirect=true";
	 }
	 public void delete(Pessoa item) throws IOException {
		 new DAO<Pessoa>(Pessoa.class).removePessoa(Integer.toString(item.getId()));;
	     this.listaPessoas.remove(item);
	  }
	 
	 public String direcionarFormSalvar() {
		 return "salvar?faces-redirect=true";
	}
	 public String cancelar() {
		 return "home?faces-redirect=true";
		
	}
}
